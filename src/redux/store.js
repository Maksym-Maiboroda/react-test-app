import { configureStore } from '@reduxjs/toolkit';
import {comments}         from './reducers/commentsReducer';

export const store = configureStore({
  reducer: {
    comments,
  },
});
