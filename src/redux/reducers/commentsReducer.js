import { createReducer } from '@reduxjs/toolkit';
import {
    fetchCommentsList,
    createComment,
    deleteComment,
    updateComment
} from '../../actions/CommentsActions';

const initialComments = {
    isLoading : false,
    commentsList  : []
};

export const comments = createReducer(initialComments, (builder) => {
    builder
        .addCase(fetchCommentsList.fulfilled, (state, action) => {
            state.commentsList = action.payload;
            state.isLoading = false;
        })
        .addCase(createComment.fulfilled, (state, action) => {
            state.commentsList = [...state.commentsList, action.payload];
        })
        .addCase(updateComment.fulfilled, (state, action) => {
            state.commentsList = state.commentsList.map(comment => comment.id === action.payload.id ? action.payload : comment);
        })
        .addCase(deleteComment.fulfilled, (state, action) => {
            state.commentsList = state.commentsList.filter(comment => comment.id !== action.payload)
        })

        // ALL REJECTED OR PENDING STATES OF ACTIONS
        .addCase(fetchCommentsList.rejected, (state) => {
            state.isLoading = false;
        })
        .addCase(fetchCommentsList.pending, (state) => {
            state.isLoading = true;
        }); 
});
