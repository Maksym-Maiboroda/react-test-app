import { connect }          from 'react-redux';
import * as commentsActions from '../../../actions/CommentsActions';
import Comments             from './Comments.js';

const mapStateToProps = state => ({
    comments  : state.comments.commentsList,
    isLoading : state.comments.isLoading
});

export default connect(mapStateToProps, {
    ...commentsActions
})(Comments);
