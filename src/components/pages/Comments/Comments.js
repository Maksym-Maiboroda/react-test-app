import { useEffect } from 'react';
import PropTypes     from 'prop-types';
import {DATA_MOCK}   from '../../../__mocks__/mockData'
import CommentCard   from '../../ui/CommentCard'
import AddComment    from '../../ui/AddComment'

const Comments = (props) => {
    const {
        fetchCommentsList,
        createComment,
        updateComment,
        deleteComment,
        addReply,
        comments,
        isLoading
    } = props;

    useEffect(() => {
        fetchCommentsList()
    }, [])

    if(isLoading) return <div>Loading...</div>

    return (
        <div>
            <AddComment 
                avatar            = {DATA_MOCK.avatar} 
                createComment     = {createComment}
                fetchCommentsList = {fetchCommentsList}
            />
            {
                comments.map((comment) => {
                    return (
                        <CommentCard 
                            {...comment} 
                            key             = {comment.id} 
                            onDeleteComment = {deleteComment} 
                            addReply        = {addReply}
                            updateComment   = {updateComment}
                        />
                    )
                })
            }
        </div>
    );
};

Comments.propTypes = {
    fetchCommentsList : PropTypes.func,
    createComment     : PropTypes.func,
    updateComment     : PropTypes.func,
    deleteComment     : PropTypes.func,
    comments          : PropTypes.array,
    isLoading         : PropTypes.bool,
};

export default Comments;
