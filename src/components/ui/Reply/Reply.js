import  { useState } from 'react';
import PropTypes from 'prop-types';
import useStyles from './styles.js';

import TextInputComponent from '../TextInputComponent'
import Button              from '../Button'

const Reply = (props) => {
    const classes = useStyles();
    const {
        id,
        avatar,
        body,
        date,
        name,
        recipient,
        updateReply,
        deleteReply
    }                                     = props;
    const [changeComment, setChangeReply] = useState(false)

    const handleDelete = () => {
        deleteReply(id)
    }

    const handleChangeReply = () => {
        setChangeReply(true)
    }

    const handleSubmitChange = (value) => {
        updateReply({
            ...value,
            id,
            avatar
        })
        setChangeReply(false)
    }

    return (
        <>
            <div className={classes.reply}>
                <div  className={classes.avatarWrap}>
                    <img alt='avatar' className={classes.avatar} src={avatar}/>
                </div>
                {changeComment ? (
                    <div className={classes.replaySection}>
                        <TextInputComponent 
                            handleSubmit = {handleSubmitChange}
                            value        = {body}
                        />
                    </div>
                ) : (
                    <div className={classes.replySection}>
                        <div className={classes.replyInfo}>
                            <div className={classes.headerReply}>
                                <div className={classes.titleReply}>{name}</div>
                                <div className={classes.recipientReply}>{`to ${recipient}`}</div>
                                <div className={classes.dateReply}>{date}</div>
                            </div>
                            <div>{body}</div>
                            <div className={classes.buttonsBlock}>
                                    <Button name='Edit' onClick={handleChangeReply}/>
                                    <Button name='Delete' onClick={handleDelete} />
                                </div>
                        </div>
                    </div>
                )}
            </div>
        </>
    );
};

Reply.propTypes = {
    avatar      : PropTypes.string,
    body        : PropTypes.string,
    date        : PropTypes.string,
    name        : PropTypes.string,
    recipient   : PropTypes.string,
    updateReply : PropTypes.func,
    deleteReply : PropTypes.func
};

export default Reply;
