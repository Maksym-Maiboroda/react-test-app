import { connect }          from 'react-redux';
import * as commentsActions from '../../../actions/CommentsActions';
import Reply                from './Reply.js';

export default connect(null, {...commentsActions})(Reply);
