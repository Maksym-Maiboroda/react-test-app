import { makeStyles, createStyles } from '@material-ui/core';

const useStyles = makeStyles(() => createStyles({
    reply : {
        display : 'flex',
        margin  : '20px 0'
    },
    avatarWrap: {
        height : 88,
        width  : 88
    },
    avatar: {
        height      : 'auto',
        width       : '100%',
        marginRight : 32
    },
    replySection: {
        display        : 'flex',
        flexDirection  : 'column',
        justifyContent : 'space-between',
        marginLeft     : 32,
        flexGrow       : 1
    },
    replyInfo: {
        height         : 88,
        display        : 'flex',
        flexDirection  : 'column',
        justifyContent : 'space-between',
    },
    headerReply: {
        display    : 'flex',
        alignItems : 'center'
    },
    titleReply: {
        fontWeight  : 600,
        fontSize    : '24px',
        lineHeight  : '29px',
        color       : '#131831',
        marginRight : 16
    },
    recipientReply: {
        color       : '#697082',
        marginRight : 16
    },
    dateReply:{
        fontSize   : '18px',
        lineHeight : '22px',
        color      : '#697082'
    },
    replaySection: {
        display : 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        marginLeft: 32,
        flexGrow: 1
    }
}));

export default useStyles;
