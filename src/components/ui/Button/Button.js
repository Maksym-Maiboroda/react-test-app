import PropTypes  from 'prop-types';
import useStyles  from './styles.js';
import classnames from 'classnames';

const Button = (props) => {
    const classes = useStyles();
    const { name, type, onClick } = props;

    return (
        <button onClick={onClick} className={classnames(classes.buttonStyle, type === 'submit' && classes.buttonSubmit)}>
            {name} 
        </button>
    );
};

Button.propTypes = {
    name    : PropTypes.string,
    type    : PropTypes.string,
    onClick : PropTypes.func
};

export default Button;
