import { makeStyles, createStyles } from '@material-ui/core';

const useStyles = makeStyles(() => createStyles({
    buttonStyle : {
        display        : 'inline-block',
        border         : 'none',
        background     : 'none',
        padding        : 0,
        margin         : '0 12px 0 0',
        textDecoration : 'none',
        color          : '#697082',
        fontSize       : '18px',
        lineHeight     : '22px',
        cursor         : 'pointer',
    },
    buttonSubmit:{
        padding      : '14px 59px',
        background   : '#3EBF8A',
        borderRadius : '3px',
        color        : '#FFFFFF',
        margin       : 0
    }
}));

export default useStyles;
