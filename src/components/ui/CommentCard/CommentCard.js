import  { useState }      from 'react';
import PropTypes          from 'prop-types';
import Button             from '../Button'
import TextInputComponent from '../TextInputComponent'
import useStyles          from './styles.js';
import Reply              from '../Reply'

const CommentCard = (props) => {
    const classes = useStyles();
    const {
        id,
        avatar,
        body,
        date,
        name,
        onDeleteComment,
        replies,
        addReply,
        updateComment
    }                                       = props;
    const [showTextInput, setShowTextInput] = useState(false)
    const [changeComment, setChangeComment] = useState(false)

    const handleClickReply = () => {
        setShowTextInput(true)
    }

    const handleDelete = () => {
        onDeleteComment(id)
    }

    const handleChangeComment = () => {
        setChangeComment(true)
    }

    const handleSubmit = (comment) => {
        addReply({
            id,
            reply: {
                ...comment,
                avatar
            }
        })
        setShowTextInput(false)
    }

    const handleSubmitChange = (value) => {
        updateComment({
            ...value,
            id,
            avatar
        })
        setChangeComment(false)
    }

    return (
        <div>
            <div className={classes.comment}>
                <div  className={classes.avatarWrap}>
                    <img alt='avatar' className={classes.avatar} src={avatar}/>
                </div>
                {changeComment ? (
                    <div className={classes.commentSection}>
                        <TextInputComponent 
                            handleSubmit = {handleSubmitChange}
                            value        = {body}
                        />
                    </div>
                ) : (
                    <div className={classes.commentSection}>
                        <div className={classes.commentInfo}>
                            <div className={classes.headerComment}>
                                <div className={classes.titleComment}>{name}</div>
                                <div className={classes.dateComment}>{date}</div>
                            </div>
                            <div>{body}</div>
                            <div className={classes.buttonsBlock}>
                                <Button name='Edit' onClick={handleChangeComment}/>
                                <Button name='Delete' onClick={handleDelete} />
                                <Button name='Reply' onClick={handleClickReply} />
                            </div>
                        </div>
                        {
                            showTextInput && 
                            <TextInputComponent 
                                recipientName={name} 
                                setShowTextInput={setShowTextInput}
                                handleSubmit={handleSubmit}
                            />
                        }
                        {
                            replies && replies.map((reply) => {
                                return <Reply {...reply} recipient={name}/>
                            })
                        }
                    </div>
                )}
            </div>
        </div>
    );
};

CommentCard.propTypes = {
    avatar          : PropTypes.string,
    body            : PropTypes.string,
    date            : PropTypes.string,
    name            : PropTypes.string,
    onDeleteComment : PropTypes.func,
    replies         : PropTypes.array,
    addReply        : PropTypes.func,
    updateComment   : PropTypes.func
};

export default CommentCard;
