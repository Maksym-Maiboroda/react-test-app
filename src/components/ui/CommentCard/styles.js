import { makeStyles, createStyles } from '@material-ui/core';

const useStyles = makeStyles(() => createStyles({
    comment : {
        display : 'flex',
        margin: '32px 80px 26px'
    },
    avatarWrap: {
        height : 120,
        width  : 120
    },
    avatar: {
        height: 'auto',
        width: '100%',
        marginRight: 32
    },
    commentSection: {
        display : 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        marginLeft: 32,
        flexGrow: 1
    },
    commentInfo: {
        height : 120,
        marginBottom: 26,
        display : 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    headerComment: {
        display    : 'flex',
        alignItems : 'center'
    },
    titleComment: {
        fontWeight : 600,
        fontSize   : '24px',
        lineHeight : '29px',
        color      : '#131831',
        marginRight: 16
    },
    dateComment: {
        fontSize   : '18px',
        lineHeight : '22px',
        color      : '#697082'
    },
    buttonsBlock:{
        display : 'flex'
    }
}));

export default useStyles;
