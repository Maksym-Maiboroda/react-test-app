import PropTypes          from 'prop-types';
import TextInputComponent from '../TextInputComponent'
import useStyles          from './styles.js';

const AddComment = (props) => {
    const classes = useStyles();
    const {
        avatar,
        createComment
    } = props;

    const handleSubmit = (comment) => {
        createComment({
           ...comment,
           avatar
        })
    }

    return (
        <div className={classes.root}>
            <div className={classes.comment}>
                <div  className={classes.avatarWrap}>
                    <img alt='avatar' className={classes.avatar} src={avatar}/>
                </div>
                <div className={classes.commentSection}>
                   <TextInputComponent handleSubmit={handleSubmit}/>
                </div>
            </div>
        </div>
    );
};

AddComment.propTypes = {
    avatar            : PropTypes.string,
    onSubmit          : PropTypes.func,
    fetchCommentsList : PropTypes.func
};

export default AddComment;
