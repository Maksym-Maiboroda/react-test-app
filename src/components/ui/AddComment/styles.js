import { makeStyles, createStyles } from '@material-ui/core';

const useStyles = makeStyles(() => createStyles({
    comment : {
        display        : 'flex',
        justifyContent : 'space-between',
        margin         : '32px 80px 26px',
        paddingBottom  : 32,
        borderBottom   : '1px solid #CDD7E5',
    },
    avatarWrap: {
        height      : 120,
        width       : 120,
        marginRight : 32
    },
    avatar: {
        height      : 'auto',
        width       : '100%',
        marginRight : 32
    },
    commentSection: {
       flexGrow: 1
    }
}));

export default useStyles;
