import React, { useState } from 'react';
import PropTypes           from 'prop-types';
import Button              from '../Button'
import useStyles           from './styles.js';
import moment              from 'moment';
import { DATE_FORMAT }     from '../../../constants'

const TextInputComponent = (props) => {
    const {
        recipientName,
        setShowTextInput,
        handleSubmit,
        value
    }                           = props;
    const classes               = useStyles();
    const [comment, setComment] = useState(value || '');

    console.log({test: value})

    const handleClickCancel = () => {
        setShowTextInput(false)
    }

    const handleChange = (e) => {
        const value = e.target.value
        
        setComment(value)
    }

    const onClickSubmit = () => {
        if (!comment.length) return;

        handleSubmit({
            name : "Terry Bator",
            body : comment,
            date : moment().format(DATE_FORMAT)
        })
        setComment('')
    }


    return (
        <div className={classes.root}>
            {
                recipientName &&
                <div className={classes.textInputHeader}>
                    <div>{`to ${recipientName}`}</div>
                    <Button name='Cancel' onClick={handleClickCancel}/>
                </div>
            }
            <textarea 
                className   = {classes.textarea} 
                placeholder = "Your message"
                onChange    = {handleChange}
                value       = {comment}
            />
            <div className={classes.submitBtnWrap}>
                <Button 
                    name    = 'Submit' 
                    type    = 'submit' 
                    onClick = {onClickSubmit}
                />
            </div>
        </div>
    );
};

TextInputComponent.propTypes = {
    recipientName    : PropTypes.string,
    setShowTextInput : PropTypes.func,
    handleSubmit     : PropTypes.func
};

export default TextInputComponent;
