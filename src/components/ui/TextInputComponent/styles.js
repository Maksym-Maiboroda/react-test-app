import { makeStyles, createStyles } from '@material-ui/core';

const useStyles = makeStyles(() => createStyles({
    textInputHeader: {
        display        : 'flex',
        justifyContent : 'space-between',
        marginBottom   : 8,
        fontSize       : '18px',
        color          : '#697082'
    },
    textarea: {
        width        : '100%',
        height       : '120px',
        outline      : 'none',
        resize       : 'none',
        padding      : '16px 24px',
        color        : '#131831',
        background   : '#FCFDFF',
        border       : '1px solid #CDD7E5',
        boxSizing    : 'border-box',
        borderRadius : '4px'
    },
    submitBtnWrap: {
        marginTop      : 16,
        display        : 'flex',
        justifyContent : 'flex-end'
    }
}));

export default useStyles;
