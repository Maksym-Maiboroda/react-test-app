import React    from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route
  }             from "react-router-dom";
import classes  from "./App.module.css";
import Comments from "./components/pages/Comments";

function App() {
    return (
        <Router>
            <div className={classes.wrapper}>
                <div className={classes.contentWrapper}>
                <Switch>
                    <Route
                    exact
                    path="/"
                    component={Comments}
                    />
                </Switch>
                </div>
            </div>
        </Router>
    );
}

export default App;
