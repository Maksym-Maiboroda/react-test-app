import axios from 'axios';

const instanse = axios.create({
  baseURL: `http://localhost:3000`
});

export const commentsAPI = {
  getComments(pageSize, currentPage) {
    return instanse
      .get(`/comments?_embed=replies`)
      .then(response => response.data);
  },

  createComment(comment) {
    return instanse
      .post(`/comments`, comment)
      .then(response => response.data);
  },

  updateComment(data) {
    return instanse
      .put(`/comments/${data.id}`, data)
      .then(response => response.data);
  },

  deleteComment(id) {
    return instanse
      .delete(`/comments/${id}`)
      .then(response => response.data);
  },

  addReply(data) {
    return instanse
      .post(`/comments/${data.id}/replies`, data.reply)
      .then(response => response.data);
  },

  updateReply(data) {
    return instanse
      .put(`/comments/replies/${data.id}`, data)
      .then(response => response.data);
  },

  deleteReply(id) {
    return instanse
      .delete(`/comments/replies/${id}`)
      .then(response => response.data);
  },
};
