import { createAsyncThunk} from '@reduxjs/toolkit';
import { commentsAPI }     from '../api/api'

export const fetchCommentsList = createAsyncThunk(
    'comments/fetchCommentsList',
    async (query) => {
        try {
            const result = await commentsAPI.getComments();
    
            return result;
        } catch (err) {
            console.warn('Error loading comments!', err);
            throw err;
        }
    }
);

export const createComment = createAsyncThunk(
    'comments/createComment',
    async (body) => {
        try {
            const result = await commentsAPI.createComment(body);
        
            return result;
        } catch (err) {
            console.warn('Error creating comment!', err);
            throw err;
        }
    }
);


export const updateComment = createAsyncThunk(
    'comments/updateComment',
    async (id, thunkAPI) => {
        try {
            const response = await commentsAPI.updateComment(id);
            
            return response;
        } catch (err) {
            console.warn('Error updating comment!', err);
            throw err;
        }
    }
);

export const deleteComment = createAsyncThunk(
    'comments/deleteComment',
    async (id) => {
        try {
            await commentsAPI.deleteComment(id);
            
            return id;
        } catch (err) {
            console.warn('Error deleting comment!', err);
            throw err;
        }
    }
);

export const addReply = createAsyncThunk(
    'comments/addReply',
    async (body, thunkAPI) => {
        const { dispatch } = thunkAPI;
        const result       = await commentsAPI.addReply(body);
        dispatch(fetchCommentsList())

        return result;
    }
);

export const updateReply = createAsyncThunk(
    'comments/updateReply',
    async (id, thunkAPI) => {
        try {
            const response = await commentsAPI.updateReply(id);
            
            return response;
        } catch (err) {
            console.warn('Error updating reply!', err);
            throw err;
        }
    }
);

export const deleteReply = createAsyncThunk(
    'comments/deleteReply',
    async (id, thunkAPI) => {
        try {
            await commentsAPI.deleteReply(id);
            
            return id;
        } catch (err) {
            console.warn('Error deleting reply!', err);
            throw err;
        }
    }
);