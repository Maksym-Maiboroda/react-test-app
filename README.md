To run this project, you need to execute several commands in the terminal.

1. To clone a project, execute the command:
`git clone git@gitlab.com: Maxim-Mayboroda / react-test-app.git`

2. Go to the folder with the project and install all the necessary dependencies by running the command:
`cd-react-test-app`
`npm install`

3. Install json server:
`npm install -g json-server`

4. Start the server:
`json-server --watch db.json`

5. Run the project:
`star npm`